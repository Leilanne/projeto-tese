from flask import jsonify, make_response, request, Flask
from flask_restful import Resource
from servicos.projetoservice import ProjetoService
import json


class ProjetoController(Resource):
    app = Flask(__name__)
    @app.route('/projeto', methods=['GET'])
    def get(self, id = None):
        try:
            if(id != None):
                response = ProjetoService.projeto().projetos_de_aprendizagem[0]
                return make_response(jsonify(response.toDict()), 200)
            else:
                response = ProjetoService.projeto().projetos_de_aprendizagem
                responseDict = [i.toDict() for i in response]
                return make_response(jsonify(responseDict), 200)
        except Exception as e:
            print(e)
            return {}, 500

