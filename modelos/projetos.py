import json
from dataclasses import dataclass
from flask import jsonify
# resource_fields = {
#     id = fields.Integer()
# questionamento = fields.Str()
# certezas = fields.List()
# duvidas = fields.List()
# recomendacoes_aceitas = fields.List()
# recomendacoes_de_amigos = fields.List()
# recomendacoes_recusadas = fields.List()
# }


class Projetos(object):
    def __init__(self, _id, questionamento, certezas, duvidas, recomendacoes_aceitas, recomendacoes_de_amigos, recomendacoes_recusadas):
        self._id = _id
        self.questionamento = questionamento
        self.certezas = certezas
        self.duvidas = duvidas
        self.recomendacoes_aceitas =recomendacoes_aceitas
        self.recomendacoes_de_amigos = recomendacoes_de_amigos
        self.recomendacoes_recusadas = recomendacoes_recusadas

    def toDict(self):
       return self.__dict__


