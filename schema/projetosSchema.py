from marshmallow import Schema, fields
from modelos.projetos import Projetos

class ProjetoSchema(Schema):
    id = fields.Integer()
    questionamento = fields.Str()
    certezas = fields.List()
    duvidas = fields.List()
    recomendacoes_aceitas = fields.List()
    recomendacoes_de_amigos = fields.List()
    recomendacoes_recusadas = fields.List()
    class Meta:
        model = Projetos