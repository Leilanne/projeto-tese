from flask import request, Flask, jsonify

from conf.dbconf import collection
from modelos.projetos import Projetos
from modelos.usuarios import *


def json_to_project(dados_pa: Projetos) -> Projetos:
    projeto = Projetos(_id=dados_pa['_id'],
                       questionamento=dados_pa['questionamento'],
                       certezas=list(dados_pa['certezas']),
                       duvidas=list(dados_pa['duvidas']),
                       recomendacoes_aceitas=[],
                       recomendacoes_de_amigos=[],
                       recomendacoes_recusadas=[]
                       )

    # if ('recomendacoes_aceitas' in dados_pa):
    projeto.recomendacoes_aceitas = list(dados_pa['recomendacoes_aceitas']) if (
            'recomendacoes_aceitas' in dados_pa) else []

    # if ('recomendacoes_de_amigos' in dados_pa):
    projeto.recomendacoes_de_amigos = list(dados_pa['recomendacoes_de_amigos']) if (
            'recomendacoes_de_amigos' in dados_pa) else []

    # if ('recomendacoes_recusadas' in dados_pa):
    projeto.recomendacoes_recusadas = list(dados_pa['recomendacoes_recusadas']) if (
            'recomendacoes_recusadas' in dados_pa) else []

    return projeto


def update_projeto(usuario):
    collection.update({'usuario.nome': usuario.nome}, {'$set': {'usuario': usuario.toJSON()}})


def usuario():
    dados_colecao = collection.find_one()
    dados_usuario = dados_colecao['usuario']
    dados_projetos_aprendizagem = dados_usuario['projetos_de_aprendizagem']
    usuario = Usuario(nome=dados_usuario['nome'], projetos_de_aprendizagem=[])
    for dados_pa in dados_projetos_aprendizagem:
        projeto_usuario = json_to_project(dados_pa)
        usuario.projetos_de_aprendizagem.append(projeto_usuario)

    return usuario


class ProjetoService:
    @staticmethod
    def projeto():
        # if (request.values == None):
        return usuario()
                # .projetos_de_aprendizagem)

    # else:
    #     self.jsonToProjeto(request.get_json())

    @staticmethod
    def lista_projetos():
        return usuario().toJSON()
