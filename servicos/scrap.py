from bs4 import BeautifulSoup
from urllib.request import urlopen
from urllib.error import HTTPError
from urllib.error import URLError
import string
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import word_tokenize
from nltk.text import TextCollection
from ScrapeSearchEngine.ScrapeSearchEngine import Google
from ScrapeSearchEngine.ScrapeSearchEngine import Bing
from modelos.recomendacoes import Recomendacao
import numpy as np

# Criando search engines:
userAgent = (
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36')  # search on google "my user agent"
search = ('Qual a quantidade de formigas em um formigueiro')  # Enter Anything for Search
colecao = []


def realiza_buscas(search):
    google = Google(search, userAgent)
    bing = Bing(search, userAgent)

    resultado = google + bing

    return set(resultado)


def preprocessa_texto(text):
    # removendo pontuações
    table = str.maketrans(dict.fromkeys(string.punctuation))
    text = text.translate(table)
    text = text.lower()

    # realizando stemming
    stemmer = SnowballStemmer('portuguese')
    text = stemmer.stem(text)

    # tokenizando
    tokens = word_tokenize(text, "portuguese")

    # possívei termos removivéis:
    termos_removiveis = ["wikipédia", "uol", "megacurioso", "enciclopédia", "wikihow", "-", "_", "mega", "curioso",
                         "wiki", "até", "facebook", "–", "as", "é", "que"]

    tokens_nao_processados = tokens[:]
    # Removendo stopwords
    for token in tokens_nao_processados:
        if token in stopwords.words('portuguese'):
            tokens.remove(token)
        elif token in termos_removiveis:
            tokens.remove(token)

    return tokens


def ler_urls(urls):
    recomendacoes = []

    for url in urls:
        # Lendo texto
        try:
            html = urlopen(url)
        except HTTPError as e:
            print(e)
        except URLError:
            print("Server down or incorrect domain")
        else:
            soup = BeautifulSoup(html.read(), "html5lib")
            # print(soup.title.text)
            if (soup.title):
                text = soup.title.get_text(strip=True)
            else:
                continue
            tokens = preprocessa_texto(text)

            colecao.append(tokens)
            recomendacoes.append(Recomendacao(url, tokens, [], 0))

    return recomendacoes


def tokeniza_duvidas(duvidas):
    duvida_t = []
    for duvida in duvidas:
        duvida_t.append(preprocessa_texto(duvida))
    return duvida_t

def remove_pdfs(urls):
    urls = list(urls)
    for url in urls:
        if ('pdf' in url[len(url) - 3:]):
            urls.remove(url)
    return urls

# duvidas = [
#     "As linhas do campo eletromagnético são tangentes? ",
#     "Como as cargas se movem em um campo eletromagnético?",
#     "Como é a unidade de medida do campo eletromagnético?"
#
# ]

def realiza_recomendacoes(duvidas):

    duvidas_tokenizadas = tokeniza_duvidas(duvidas)

    texto_duvidas = TextCollection(duvidas_tokenizadas)

    dic_token_duvidas = []

    for dt in duvidas_tokenizadas:
        dic_temp = []
        dic_temp = dict.fromkeys(dt, 0)
        for termo in dt:
            dic_temp[termo] = texto_duvidas.tf_idf(termo, dt)
        dic_token_duvidas.append(dic_temp)

    pesquisa = sorted(dic_token_duvidas, key=lambda dic: sum(dic.values()))

    termos_pesquisados = ' '.join(str(key) for key in pesquisa[0].keys())
    pesos_busca = list(pesquisa[0].values())
    urls = realiza_buscas(termos_pesquisados)

    urls = remove_pdfs(urls)
    recomendacoes = ler_urls(urls)

    textos = TextCollection(colecao)
    for recomendacao in recomendacoes:
        recomendacao.tfidf = dict.fromkeys(recomendacao.token, 0)
        for token in recomendacao.token:
            recomendacao.tfidf[token] = textos.tf_idf(token, recomendacao.token)

    for recomendacao in recomendacoes:
        pesos_doc = list(recomendacao.tfidf.values())
        somatorio = 0
        for peso in pesos_doc:
            for pq in pesos_busca:
                somatorio = somatorio + (peso * pq)
        recomendacao.sim = np.cos(somatorio / (np.sqrt(sum(np.power(pesos_doc, 2))) * np.sqrt(sum(np.power(pesos_busca, 2)))))

    possiveis_recomendacoes = sorted(recomendacoes, key=lambda recomendacao: recomendacao.sim)

    return possiveis_recomendacoes;
