from flask import Flask
from flask_restful import Api
from flask_wtf.csrf import CSRFProtect

from conf.dbconf import collection
from controllers.projetocontroller import ProjetoController
from modelos.usuarios import *

# configuration
DEBUG = True





dados_colecao = collection.find_one()
dados_usuario = dados_colecao['usuario']
dados_projetos_aprendizagem = dados_usuario['projetos_de_aprendizagem']

usuario = Usuario(nome = dados_usuario['nome'], projetos_de_aprendizagem=[])
# instacia o app
csrf = CSRFProtect()
app = Flask(__name__)
app.config.from_object(__name__)
csrf.init_app(app)
api = Api(app)
# CORS(app, resources={r'/*': {'origins': '*'}})
# chamando rotas
routes = [
    '/projeto',
    '/projetos',
]
api.add_resource(ProjetoController, '/projetos','/projetos/<int:id>', methods=['GET', 'POST'])
# habilita o CORS

# app.config.from_object('config')  # Configuring from Python Files




if __name__ == '__main__':
    projetos_de_aprendizagem = ProjetoController()
    app.run(debug=True, port=5000)

# def jsonToProjeto(dados_pa):
#     projeto = Projetos(_id=dados_pa['_id'],
#                        questionamento=dados_pa['questionamento'],
#                        certezas=list(dados_pa['certezas']),
#                        duvidas=list(dados_pa['duvidas']),
#                        recomendacoes_aceitas=[],
#                        recomendacoes_de_amigos=[],
#                        recomendacoes_recusadas=[]
#                        )
#
#     if ('recomendacoes_aceitas' in dados_pa):
#         projeto.recomendacoes_aceitas = list(dados_pa['recomendacoes_aceitas'])
#
#     if ('recomendacoes_de_amigos' in dados_pa):
#         projeto.recomendacoes_de_amigos = list(dados_pa['recomendacoes_de_amigos'])
#
#     if ('recomendacoes_recusadas' in dados_pa):
#         projeto.recomendacoes_recusadas = list(dados_pa['recomendacoes_recusadas'])
#
#     return projeto
#
# for dados_pa in dados_projetos_aprendizagem:
#     projeto_usuario = jsonToProjeto(dados_pa)
#     usuario.projetos_de_aprendizagem.append(projeto_usuario)
#
# def update_projeto(usuario):
#     collection.update({'usuario.nome': usuario.nome}, {'$set': {'usuario': usuario.toJSON()}})
#
#
#
#
#
# @app.route('/')
# def hello_world():
#     return 'Hello World!'
#
# @app.route('/mongo')
# def objeto_mongo():
#     return usuario.toJSON()

# @app.route('/projeto')
# def projeto():
#     if(request.get_json() == None):
#         return usuario.projetos_de_aprendizagem[0].toJSON()
#     else:
#         projeto = jsonToProjeto(request.get_json())



# @app.route('/projeto/altera/', methods=['PUT'])
# def update_projeto():
#     projeto = jsonToProjeto(request.get_json())
#
#     for p in usuario.projetos_de_aprendizagem:
#
#         if(int(p._id) == projeto._id):
#             index = usuario.projetos_de_aprendizagem.index(p)
#             usuario.projetos_de_aprendizagem[index] = projeto
#
#     update_projeto(usuario)
#     response_object = {'status': 'success'}
#     return response_object
#
# @app.route('/projeto/recomendacoes', methods=['GET'])
# def recomendacoes():
#     projeto = jsonToProjeto(request.get_json())
#     recomendacoes = scrap.realiza_recomendacoes(projeto.duvidas)
#
#     return jsonify(recomendacoes)
#
# @app.route('/projetos', methods=['GET'])
# def lista_projetos():
#     return usuario.toJSON()


